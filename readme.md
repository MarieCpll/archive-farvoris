# Mes Liens favoris pour les veilles, classés par sujets. Bonne Lecture !

## Javascript
### *à installer pour ton environnement de travail*
- https://nodejs.org/en/
- https://www.npmjs.com/
### *Framework*
- Presentaion Gulp : https://ldevernay.github.io/green/2019/08/13/eco-gulp.html
- Gulp officiel : https://gulpjs.com/docs/en/getting-started/quick-start
### *Outils*
- site de Test : https://jestjs.io/

## SQL

## PHP
### *Cours*
- https://github.com/simplonco/php-training/blob/master/courses/Tutoriel%20PHP%20-%20Introduction.pdf
### *Exercices :*
- https://github.com/SimplonReunion/php-cours-activites

## Install

## Command

## HTML/CSS 

## Outils

- Navigateur noir : https://addons.mozilla.org/fr/firefox/addon/darkreader/

- Tous les onglets en un seul : https://addons.mozilla.org/fr/firefox/addon/onetab/

- Organiser ses tâches (système gestion de projet) : Trello 

- Diagramme d'activité/UML : https://www.lucidchart.com
